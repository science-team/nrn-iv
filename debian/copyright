Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: InterViews
Source: https://github.com/neuronsimulator/iv

Files: *
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: debian/*
Copyright: 2022-2023, Matthias Klumpp <mak@debian.org>
License: BSD-3-Clause

Files: src/bin/*
Copyright: 1989-1991, Stanford University
License: BSD-3-Clause

Files: src/bin/idemo/main.cpp
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/include/Dispatch/*
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/include/Dispatch/_defines.h
  src/include/Dispatch/_gendefs
  src/include/Dispatch/_undefs.h
Copyright: Michael Hines
           John W. Moore
           Ted Carnevale
License: BSD-3-Clause

Files: src/include/IV-2_6/InterViews/*
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/include/IV-2_6/InterViews/ihandler.h
Copyright: 1991, 1992, Stanford University
  1991, 1992, Silicon Graphics, Inc.
License: BSD-3-Clause

Files: src/include/IV-Mac/bitmap.h
  src/include/IV-Mac/brush.h
  src/include/IV-Mac/font.h
  src/include/IV-Mac/raster.h
Copyright: 1993, Tim Prinzing
License: BSD-3-Clause

Files: src/include/IV-Mac/session.h
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/include/IV-Win/*
Copyright: 2002, Tim Prinzing, Michael Hines
  1993, Tim Prinzing
License: LGPL-2+

Files: src/include/IV-Win/mprinter.h
Copyright: 2002, Michael Hines
License: LGPL-2+

Files: src/include/IV-X11/*
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/include/IV-X11/Xdefs.h
  src/include/IV-X11/Xundefs.h
Copyright: Michael Hines
           John W. Moore
           Ted Carnevale
License: BSD-3-Clause

Files: src/include/IV-X11/xdrag.h
Copyright: 1991-1993, Redwood Design Automation
License: BSD-3-Clause

Files: src/include/IV-X11/xselection.h
Copyright: 1991, 1992, Stanford University
  1991, 1992, Silicon Graphics, Inc.
License: BSD-3-Clause

Files: src/include/IV-look/*
Copyright: 1991, 1992, Stanford University
  1991, 1992, Silicon Graphics, Inc.
License: BSD-3-Clause

Files: src/include/IV-look/button.h
  src/include/IV-look/choice.h
  src/include/IV-look/menu.h
  src/include/IV-look/stepper.h
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/include/IV-look/mw_kit.h
Copyright: 1993, Tim Prinzing
License: BSD-3-Clause

Files: src/include/IV-look/telltale.h
Copyright: Michael Hines
           John W. Moore
           Ted Carnevale
License: BSD-3-Clause

Files: src/include/InterViews/*
Copyright: 1991, 1992, Stanford University
  1991, 1992, Silicon Graphics, Inc.
License: BSD-3-Clause

Files: src/include/InterViews/_defines.h
  src/include/InterViews/_gendefs
  src/include/InterViews/_undefs.h
Copyright: Michael Hines
           John W. Moore
           Ted Carnevale
License: BSD-3-Clause

Files: src/include/InterViews/drag.h
  src/include/InterViews/iv3text.h
Copyright: 1991-1993, Redwood Design Automation
License: BSD-3-Clause

Files: src/include/InterViews/winbmp.h
Copyright: 1993, Tim Prinzing
License: BSD-3-Clause

Files: src/include/OS/*
Copyright: 1991, 1992, Stanford University
  1991, 1992, Silicon Graphics, Inc.
License: BSD-3-Clause

Files: src/include/OS/_defines.h
  src/include/OS/_gendefs
  src/include/OS/_undefs.h
Copyright: Michael Hines
           John W. Moore
           Ted Carnevale
License: BSD-3-Clause

Files: src/include/OS/dirent.h
Copyright: 1993, Tim Prinzing
License: BSD-3-Clause

Files: src/include/OS/enter-scope.h
  src/include/OS/leave-scope.h
  src/include/OS/list.h
  src/include/OS/table.h
  src/include/OS/table2.h
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/include/TIFF/*
Copyright: 1991, 1992, Silicon Graphics, Inc.
  1988-1992, Sam Leffler
License: BSD-3-Clause

Files: src/include/TIFF/format.h
Copyright: 1991, 1992, Stanford University
  1991, 1992, Silicon Graphics, Inc.
License: BSD-3-Clause

Files: src/include/Unidraw/*
Copyright: 1989-1991, Stanford University
License: NTP~disclaimer

Files: src/include/Unidraw/_defines.h
  src/include/Unidraw/_gendefs
  src/include/Unidraw/_undefs.h
Copyright: Michael Hines
           John W. Moore
           Ted Carnevale
License: BSD-3-Clause

Files: src/include/Unidraw/enter-scope.h
  src/include/Unidraw/leave-scope.h
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/lib/*
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/lib/IV-Mac/*
Copyright: Michael Hines
           John W. Moore
           Ted Carnevale
License: BSD-3-Clause

Files: src/lib/IV-Mac/bitmap.cpp
  src/lib/IV-Mac/brush.cpp
  src/lib/IV-Mac/font.cpp
  src/lib/IV-Mac/raster.cpp
Copyright: 1993, Tim Prinzing
License: BSD-3-Clause

Files: src/lib/IV-Mac/canvas.cpp
Copyright: MESSAGE: / 1991, Silicon Graphics, Inc. / 1987-1991, Stanford University
License: BSD-3-Clause

Files: src/lib/IV-Mac/session.cpp
  src/lib/IV-Mac/window.cpp
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/lib/IV-Win/*
Copyright: 2002, Tim Prinzing, Michael Hines
  1993, Tim Prinzing
License: LGPL-2+

Files: src/lib/IV-Win/ivclean.cpp
  src/lib/IV-Win/mprinter.cpp
Copyright: 2002, Michael Hines
License: LGPL-2+

Files: src/lib/IV-X11/glcontext.cpp
  src/lib/IV-X11/xselection.cpp
Copyright: 1991, 1992, Stanford University
  1991, 1992, Silicon Graphics, Inc.
License: BSD-3-Clause

Files: src/lib/IV-X11/xdrag.cpp
Copyright: 1991-1993, Redwood Design Automation
License: BSD-3-Clause

Files: src/lib/InterViews/*
Copyright: 1991, 1992, Stanford University
  1991, 1992, Silicon Graphics, Inc.
License: BSD-3-Clause

Files: src/lib/InterViews/iv3text.cpp
Copyright: 1991-1993, Redwood Design Automation
License: BSD-3-Clause

Files: src/lib/InterViews/winbmp.cpp
Copyright: 1993, Tim Prinzing
License: BSD-3-Clause

Files: src/lib/OS/*
Copyright: 1991, 1992, Stanford University
  1991, 1992, Silicon Graphics, Inc.
License: BSD-3-Clause

Files: src/lib/OS/dirent.cpp
Copyright: 1993, Tim Prinzing
License: BSD-3-Clause

Files: src/lib/OS/host.cpp
  src/lib/OS/memory.cpp
  src/lib/OS/string.cpp
  src/lib/OS/ustring.cpp
Copyright: 1991, Silicon Graphics, Inc.
  1987-1991, Stanford University
License: BSD-3-Clause

Files: src/lib/TIFF/*
Copyright: 1991, 1992, Silicon Graphics, Inc.
  1988-1992, Sam Leffler
License: BSD-3-Clause

Files: src/lib/TIFF/HOWTO
  src/lib/TIFF/VERSION
  src/lib/TIFF/mkspans.c
Copyright: Michael Hines
           John W. Moore
           Ted Carnevale
License: BSD-3-Clause

Files: src/lib/Unidraw/*
Copyright: 1989-1991, Stanford University
License: BSD-3-Clause

Files: src/lib/app-defaults/*
Copyright: Michael Hines
           John W. Moore
           Ted Carnevale
License: BSD-3-Clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * The name of the author may be used to endorse or promote products
   derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by the
 Free Software Foundation; version 2 of the License, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU Library
 General Public License can be found in `/usr/share/common-licenses/LGPL-2'.

License: NTP~disclaimer
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided
 that the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation, and that the name of Stanford not be used in advertising or
 publicity pertaining to distribution of the software without specific,
 written prior permission.  Stanford makes no representations about
 the suitability of this software for any purpose.  It is provided "as is"
 without express or implied warranty.
 .
 STANFORD DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 IN NO EVENT SHALL STANFORD BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
